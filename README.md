# Class Stuff
## Things I do, read or create as a teacher

This repo is going to be my drawer for storing stuff I do or collect for classes. Currently preparing a robotics class for kids, but I'm going to share more projects.

## Current projects

* Privacy guide for public schools in Spanish
* Classroom organizer tool in python
* Robotics for kids content and activities



