import os

students = [["name","age","no","A","example"],["name2","age2","no","B","example"]]
activities1 = [["path","sefsffeff"],["prueba","prueba2"]]
activities2 = [["path advanced","sefsefawef"],["prueba","prueba2"]]

def menu():
    print('Choose your option: \n')
    print('1. Add a new student')
    print('2. Add first class activity')
    print('3. Add second class activity')
    #print('4. Check students in class A')
    #print('5. Check students in class B')
    print('6. Exit\n')

def class_(class_letter):
    c = 0
    for i in students:
        print(students[i][3])
        if students[i][3] == class_letter:
            print("Name: ")
            print(students[i][0])
            c = c + 1
    print("\nTotal students in this group: ")
    print(c)


def nueva_act(level):

    aname = raw_input('Enter activity name: ')
    des = raw_input('Enter description: ')

    new_act = [aname, des]

    if level == 1:
        activities1.append(new_act)
        save_act1()
    elif level == 2:
        activities2.append(new_act)
        save_act2()
    else:
        print("Incorrect level value")

    save_student()


def nuevo_estudiante():

    name = raw_input('Enter student name: ')
    age = raw_input('Enter student age: ')
    sabe = raw_input('Can the student program already?: ')
    group = raw_input('Group: ')
    comments = raw_input('Comments: ')

    new_student = [name, age, sabe, group, comments]

    students.append(new_student)

    save_student()

def save_student():

    file = open("estudiantes.txt","a")
    tam = len(students)
    file.write("\n")
    file.write(students[tam-1][0])
    file.write(" has ")
    file.write(students[tam-1][1])
    file.write(" years old. This student ")
    file.write(students[tam-1][2])
    file.write(" program. This student is in group ")
    file.write(students[tam-1][3])
    file.write("\nComments about this student:\n")
    file.write(students[tam-1][4])
    file.write("\n")


    file.close()

def save_act1():

    file = open("actividades_basic.txt","a")
    tam2 = len(activities1)
    file.write("Name:\n")
    file.write(activities1[tam2-1][0])
    file.write("\n")
    file.write("Description:\n")
    file.write(activities1[tam2-1][1])
    file.write("\n\n")
    file.close()

def save_act2():

    file = open("actividades_avanzadas.txt","a")
    tam3 = len(activities2)
    file.write("Name:\n")
    file.write(activities2[tam3-1][0])
    file.write("\n")
    file.write("Description:\n")
    file.write(activities2[tam3-1][1])
    file.write("\n\n")
    file.close()

menu()
option = raw_input('\nSelect option: ')

while option != "6":

    if option == "1":
        nuevo_estudiante()
    elif option == "2":
        nueva_act(1)
    elif option == "3":
        nueva_act(2)
    elif option == "4":
        class_("A")
    elif option == "5":
        class_("B")
    else:
        print("\nSomething didn't work.")

    os.system('clear')
    menu()
    option = raw_input('\nSelect option: ')

print("\nSee you!")